public class Main {

    static int i = 0;
    public static void recursion(){
        i++;
        if (i % 5 == 0 && i % 3 ==0 ) {
            System.out.println("FizzBuzz");
            i++;}
        if (i % 3 == 0) {
            System.out.println("Fizz");
            i++;}

        if (i % 5 == 0) {
            System.out.println("Buzz");
            i++;}

        if (i <= 100) {
            System.out.println(i);
            recursion();
        }
    }
    public static void main(String[] args){
        recursion();
    }
}

