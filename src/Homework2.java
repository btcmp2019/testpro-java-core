import java.util.Locale;
import java.util.Scanner;

public class Homework2 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Input the word/phrase: ");
		String text = input.nextLine();
		String cleaned = text.replaceAll("[^a-zA-Z0-9]", "");
		String lowercaseCleaned = cleaned.toLowerCase();
		boolean pal = true;
		for (int i = 0; i < lowercaseCleaned.length() / 2 + 1; i++) {
			if (lowercaseCleaned.toCharArray()[i] != lowercaseCleaned.toCharArray()[lowercaseCleaned.length()- i - 1]) {
				pal = !pal;
				break;
			}
		}
			
		System.out.println(pal ? "Word/phrase is palindrome: " + text : "Word/phrase is not palindrome: " + text);
		
	}
}